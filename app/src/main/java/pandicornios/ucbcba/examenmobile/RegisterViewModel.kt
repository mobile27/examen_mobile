package pandicornios.ucbcba.examenmobile

import android.os.Handler
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class RegisterViewModel(val registerRepository: RegisterRepository): ViewModel() {
    val model: LiveData<UiModel>
        get()=_model
    private val _model= MutableLiveData<UiModel>()

    sealed class UiModel{
        class Register(val success:Boolean): UiModel()
        object Loading:UiModel()

    }
    fun doRegister(name:String, lastname:String, email:String){
        val user = User(name,lastname,email)
        _model.value=UiModel.Loading
        val runnable= Runnable {
            _model.value= UiModel.Register(registerRepository.saveUser(user))
        }
        Handler().postDelayed(runnable,3000)
    }
}