package pandicornios.ucbcba.examenmobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var registerViewModel: RegisterViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        registerViewModel=RegisterViewModel(RegisterRepository())

        registerViewModel.model.observe(this, Observer  (::updateUi))
        button.setOnClickListener{
            registerViewModel.doRegister(name.text.toString(), lastname.text.toString(), email.text.toString())
        }
    }
    private fun updateUi(model: RegisterViewModel.UiModel?) {
        loading_progress_bar.visibility=if (model is RegisterViewModel.UiModel.Loading) View.VISIBLE else View.GONE
        when (model ){
            is RegisterViewModel.UiModel.Register->validarLogin(model.success)
        }

    }
    private fun validarLogin(resp:Boolean){
        if(resp ){
            Toast.makeText(this,"REGISTRO EXITOSO", Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(this,"ERROR", Toast.LENGTH_LONG).show()
        }

    }
}
